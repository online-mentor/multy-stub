const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const session = require('express-session')

const app = express()
const cors = require('cors')
require('dotenv').config()

const config = require('../.serverrc')
const { setIo } = require('./io')

app.use(cookieParser())
app.options('*', cors())
app.use(cors())
const server = setIo(app)

const sess = {
    secret: 'super-secret-key',
    resave: true,
    saveUninitialized: true,
    cookie: {
    },
}
if (app.get('env') === 'production') {
    app.set('trust proxy', 1)
    sess.cookie.secure = true
}
app.use(session(sess))

app.use(bodyParser.json({
    limit: '50mb',
}))
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
}))
app.use(require('./root'))

/**
 * Добавляйте сюда свои routers.
 */
app.use('/example', require('./routers/example'))
// app.use('/coder', require('./routers/coder'))
app.use('/stc-21-03', require('./routers/stc-21-03'))
app.use('/stc-21', require('./routers/stc'))
app.use('/stc-22-24', require('./routers/stc-22-24'))
// app.use('/bushou-api', require('./routers/bushou'))

// app.use('/uryndyklar-api', require('./routers/uryndyklar'))
// app.use('/neptunium', require('./routers/neptunium'))
// app.use('/music-learn', require('./routers/music-learn'))
// app.use('/publicium', require('./routers/publicium'))
// app.use('/task-boss', require('./routers/task-boss'))
// app.use('/car-wash', require('./routers/car-wash'))
app.use('/zoom-bar', require('./routers/zoom-bar'))
app.use('/basket', require('./routers/basket'))
app.use('/easy-project', require('./routers/easy-project'))
app.use('/sugarbun', require('./routers/sugarbun'))
require('./routers/hub-video')

app.use(require('./error'))

server.listen(config.port, () => console.log(`Listening on http://localhost:${config.port}`))
