const router = require('express').Router()

let labels = [
    {
        color: 'D50101',
        title: 'Важное',
    },
    {
        color: 'F4511E',
        title: 'Дом',
    },
    {
        color: 'F5BF25',
        title: 'Работа',
    },
    {
        color: '0B8043',
        title: 'Дача',
    },
    {
        color: '0C9CE5',
        title: 'Аниме',
    },
    {
        color: 'E67C73',
        title: 'Синий',
    },
    {
        color: '33B679',
        title: 'Грабежи',
    },
    {
        color: '8E24AA',
        title: 'Оранжевый',
    },
]

const todos = [
    {
        id: 0,
        completed: false,
        title: 'Съесть деда',
        labels: [
            {
                color: 'D50101',
                title: 'Важное',
            },
            {
                color: 'F4511E',
                title: 'Дом',
            },
        ],
        description: 'Lorem ipsum dolor sit amet.',
        completeDate: 567800000000,
    },
    {
        id: 1,
        completed: false,
        title: 'гав',
        labels: [
            {
                color: '0B8043',
                title: 'Дача',
            },
            {
                color: '0C9CE5',
                title: 'Аниме',
            },
            {
                color: 'F5BF25',
                title: 'Работа',
            },
        ],
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, laboriosam.',
        completeDate: 1997800000000,
    },
    {
        id: 2,
        completed: true,
        title: 'Мяу',
        labels: [
            {
                color: 'E67C73',
                title: 'Синий',
            },
            {
                color: '33B679',
                title: 'Грабежи',
            },
            {
                color: '8E24AA',
                title: 'Оранжевый',
            },
        ],
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, laboriosam.',
        completeDate: 1997800000000,
    },
    {
        id: 3,
        completed: false,
        title: 'Неудаляемая',
        labels: [
            {
                color: 'E67C73',
                title: 'Синий',
            },
            {
                color: '33B679',
                title: 'Грабежи',
            },
            {
                color: '8E24AA',
                title: 'Оранжевый',
            },
        ],
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, laboriosam.',
        completeDate: 1997800000000,
    },
]

const ErrorHandler = (error, _req, res) => {
    const statusCode = res.statusCode || 500
    res.status(statusCode)
    res.json({
        message: error.message,
        stack: process.env.NODE_ENV === 'production' ? '🥑' : error.stack,
    })
}

try {
    router.post('/signup', (_req, _res, next) => {
        setTimeout(next, 1000)
    }, (_req, res) => {
        res.status(200).send({
            accessToken: 'jhbdhbfsuhbvhsfbvuhsbfuvhsbfu',
        })
    })

    router.post('/login', (_req, _res, next) => {
        setTimeout(next, 1000)
    }, (req, res) => {
        if (req.body.password !== 'pass') res.status(401).send('Wrong password!')
        else {
            res.status(200).send({
                accessToken: 'lfhjvbfealihvbsfjlh',
            })
        }
    })

    router.get('/user', (_req, res) => {
        res.send({
            username: 'stalin',
            email: 'vozhd@kgb.ussr',
        })
    })

    router.put('/user/username', (req, res) => {
        if (req.body.username === 'error') {
            res.status(500).send()
        } else {
            res.status(201).send()
        }
    })

    router.put('/user/password', (_req, res) => {
        res.status(201).send()
    })

    router.get('/labels', (_req, res) => {
        res.send(labels)
    })

    router.patch('/label', (_req, res) => {
        res.status(201).send()
    })

    router.put('/labels', (_req, res) => {
        res.status(201).send()
    })

    router.get('/todos', (_req, res) => {
        res.send({
            todos,
        })
    })

    router.post('/todo', (_req, _res, next) => {
        setTimeout(next, 1000)
    }, (req, res) => {
        if (req.body.title === 'error') {
            res.status(500).send()
        } else {
            res.status(204).send()
        }
    })

    router.put('/todo', (_req, _res, next) => {
        setTimeout(next, 300)
    }, (req, res) => {
        if (req.body.todo.title === 'error' || req.body.todo.title === 'Неудаляемая') {
            res.status(500).send()
        } else {
            res.status(201).send()
        }
    })

    router.delete('/todo', (_req, _res, next) => {
        setTimeout(next, 300)
    }, (req, res) => {
        if (req.body.id === 3) {
            res.status(500).send()
        } else {
            res.status(204).send()
        }
    })

    router.patch('/todo', (_req, res) => {
        res.status(204).send()
    })

    router.use(ErrorHandler)
} catch (e) {
    console.error(e)
}

module.exports = router
