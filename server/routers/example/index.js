const router = require('express').Router()

const first = router.get('/first', (req, res) => {
    res.send({
        success: true,
        warnings: [{
            title: 'Внимание',
            text: 'Данный api создан для примера!',
        }],
    })

    /**
     * Этот эндпоинт будет доступен по адресу http://89.223.91.151:8080/multystub/example/first
     */
})

router.use('/example-api', first)

module.exports = router
