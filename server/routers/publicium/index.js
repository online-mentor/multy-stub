const { Router, json } = require('express')
const cors = require('cors')

const router = Router()

router.use(json())

if (process.env.NODE_ENV !== 'production') {
    router.use(cors())
}

router.get('/games/trending', (req, res) => res.status(200).send(require('./mocks/games/trending.json')))

router.get('/users/top-load', (req, res) => res.status(200).send(require('./mocks/users/top-load.json')))

router.get('/maps/trending', (req, res) => res.status(200).send(require('./mocks/maps/trending.json')))

router.get('/search', (req, res) => res.status(200).send(require('./mocks/search.json')))

router.get('/games', (req, res) => res.status(200).send(require('./mocks/games.json')))
router.get('/games/:id', (req, res) => {
    const games = require('./mocks/games.json')

    const game = games.find((game) => game.id === Number.parseInt(req.params.id, 10))

    if (!game) {
        return res.status(404).send({
            code: -3,
            message: 'Game not found',
        })
    }

    res.status(200).send(game)
})

router.get('/users/:id', (req, res) => res.status(200).send(require('./mocks/users/12.json')))

router.get('/users/:id/collection', (req, res) => res.status(200).send(require('./mocks/users/12/collection.json')))

router.get('/maps/by/:id', (req, res) => res.send(require('./mocks/maps/by/id.json')))

module.exports = router
