const checkRequiredMidleware = (options) => (req, res, next) => {
    // eslint-disable-next-line no-restricted-syntax
    for (const option of options) {
        if (!req.body[option]) {
            res.status(400).send(`Required field ${option} is not present`)
            return
        }
    }
    next()
}

module.exports = checkRequiredMidleware
