const _idToId = (data) => {
    const { _id, ...rest } = data

    return {
        id: _id,
        ...rest,
    }
}

const _idToIdArray = (data) => {
    const _idToIdMap = data.map((item) => _idToId(item))

    return _idToIdMap
}

const getResponse = (error, data, success = true) => {
    if (error) {
        return {
            success: false,
            error,
        }
    }

    return {
        success,
        data,
    }
}

module.exports = {
    getResponse,
    _idToIdArray,
    _idToId,
}
