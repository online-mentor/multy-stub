const reservationRouter = require('express').Router()
const { getTabs, updateTab } = require('../controller')

reservationRouter.get('/', async (req, res) => {
    let error = null
    const tabData = await getTabs().catch((e) => error = e.message)
    if (error) {
        res.status(400).send(error)
    } else {
        res.status(200).send(tabData)
    }
})

reservationRouter.post('/post', async (req, res) => {
    let error = null
    const data = req.body
    console.log(data.length)
    const promises = []
    for (let i = 0; i < data.length; i++) {
        const item = data[i]
        const result = updateTab(item.id, item.timeStamps)
        promises.push(result)
    }
    await Promise.all(promises).catch((e) => error = e.message)
    if (error) {
        res.status(400).send(error)
    } else {
        res.status(200).send()
    }
})

module.exports = reservationRouter
