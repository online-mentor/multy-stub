const dishRouter = require('express').Router()
const ObjectId = require('mongodb').ObjectID
const { getResponse } = require('../utils/common')
const { getDish, getTabs, getDishById, getDishByCategory } = require('../controller')

dishRouter.get('/', async (req, res) => {
    let error = null
    const dishData = await getDish().catch((e) => error = e.message)
    res.send(getResponse(error, dishData))
})

dishRouter.get('/reservation', async (req, res) => {
    let error = null
    const tabData = await getTabs().catch((e) => error = e.message)
    res.send(getResponse(error, tabData))
})

dishRouter.get('/alsolike/:dishId', async (req, res) => {
    let error = null
    const dish = await getDishById({ id: req.params.dishId }).catch((e) => error = e.message)
    const categoryArr = await getDishByCategory({
        cat: dish.category, id: req.params.dishId,
    }).catch((e) => error = e.message)
    res.status(200).send(getResponse(error, categoryArr))
})

dishRouter.get('/:dishId', async (req, res) => {
    let error = null
    const dishData = await getDishById({ id: req.params.dishId }).catch((e) => error = e.message)
    res.send(getResponse(error, dishData))
})

module.exports = dishRouter
