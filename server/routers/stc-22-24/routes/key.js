const JWT_TOKEN = 'secret'
const SALT = 3

module.exports = {
    JWT_TOKEN,
    SALT,
}
