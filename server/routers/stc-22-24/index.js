const router = require('express').Router()

router.use('/auth', require('./routes/auth'))
router.use('/dish', require('./routes/dish'))
router.use('/reservation', require('./routes/reservation'))
router.use('/order', require('./routes/order'))

module.exports = router
