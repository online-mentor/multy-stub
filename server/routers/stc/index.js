const router = require('express').Router()

const { connect, getFilmData, setLike, setRating } = require('./controllers')
const { getAnswer } = require('../../utils/common')

connect()

router.get('/film/:id', async (req, res) => {
    const data = await getFilmData(req.params.id)
    res.send(getAnswer(null, data))
})

router.post('/film/:id/rating', async (req, res) => {
    const filmData = await setRating({
        filmId: req.params.id, rating: req.body.rating,
    })
    res.send(getAnswer(null, filmData))
})

router.post('/film/:id/like', async (req, res) => {
    const filmData = await setLike(req.params.id)
    res.send(getAnswer(null, filmData))
})

const first = router.all('/feedback', (req, res) => {
    setTimeout(() => {
        res.send({
            success: true,
            body: {
                ...req.query, ...req.body,
            },
        })
    }, 1000)

    /**
     * Этот эндпоинт будет доступен по адресу http://inno-ijl.ru/multystub/stc-21-03/feedback
     */
})

function idGen() {
    let i = 1
    return () => i++
}

const getId = idGen()

let posts = [
    {
        id: getId(),
        link: 'https://www.unian.net/russianworld/v-rossii-na-bereg-zaliva-vybrosilo-tonny-ikry-mestnye-udobryayut-eyu-ogorody-video-novosti-rossii-11420710.html',
        header: 'В России на берег залива выбросило тонны икры: местные удобряют ею огороды (видео)',
        poster: 'images/1.jpeg',
        text: `На побережье залива Анива в Сахалинской области РФ штормом выбросило тонны икры тихоокеанской сельди сахалино-хоккайдского стада. Об этом на своей странице в Facebook рассказал глава общественной организации "Экологическая вахта Сахалина" Дмитрий Лисицын.
        По его словам, икру косяки сельди ранее отметали недалеко от берега, а теперь ее сплошная полоса шириной в несколько метров растянулась по побережью примерно на 3 км. Толщина слоя икры местами достигает 10-15 см.`,
    },
    {
        id: getId(),
        link: 'https://www.unian.net/curiosities/zhenshchina-postirala-vyigryshnyy-bilet-na-26-millionov-dollarov-poslednie-novosti-11420341.html',
        header: 'Женщина постирала выигрышный билет на 26 миллионов долларов',
        poster: 'images/2.webp',
        text: `Американка случайно постирала со штанами выигрышный лотерейный билет на 26 миллионов долларов.
Женщина приобрела лотерею еще в ноябре в пригороде Лос-Анджелеса. Однако за выигрышем она пришла тогда, когда истекал крайний срок – через 180 дней. В магазине американка отметила, что не может показать билет, поскольку постирала его в кармане одежды.
В то же время представитель калифорнийской лотереи Кэти Джонстон заявила, что отснятого материала (имея в виду кадры с камер видеонаблюдения, на которые попала покупатель) недостаточно, чтобы выдать призовые деньги женщине.`,
    },
]

const getPosts = (req, res) => {
    setTimeout(() => {
        res.send({
            success: true, body: posts,
        })
    }, 1000)

    /**
     * Этот эндпоинт будет доступен по адресу http://89.223.91.151:8080/multystub/stc-21-03/posts
     */
}

const addPost = (req, res) => {
    posts.push({
        id: getId(), ...req.body,
    })
    setTimeout(() => {
        res.send({
            success: true, body: posts,
        })
    }, 1000)
}

router.get('/posts', getPosts)
router.post('/posts', addPost)
router.use('/', first)

module.exports = router
