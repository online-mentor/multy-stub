const router = require('express').Router()
const { getIngredients, getCakeshape, getCakesweight, getStats, getCakeitem } = require('./controller')

module.exports = router

router.get('/constructor/formitems', async (req, res) => {
    // получение данных из бэкенда
    const ingredient = (await getIngredients()).map((item) => ({
        ...item,
        _id: undefined,
    }))
    const cakeshape = (await getCakeshape()).map((item) => ({
        ...item,
        _id: undefined,
    }))
    const cakesweight = (await getCakesweight()).map((item) => ({
        ...item,
        _id: undefined,
    }))
    res.send({
        ingredients: ingredient,
        cakeshapes: cakeshape,
        cakesweights: cakesweight,
    })
})

router.get('/admin/adminstats', async (req, res) => {
    // получение данных из бэкенда
    const adminstat = (await getStats()).map((item) => ({
        ...item,
        _id: undefined,
    }))
    res.send({
        adminstats: adminstat,
    })
})
router.post('/constructor/submit', (req, res) => {
    res.send()
})

router.get('/menu/cakeitems', async (req, res) => {
    // получение данных из бэкенда
    res.send(require('./collections/cakeitem.json'))
})
