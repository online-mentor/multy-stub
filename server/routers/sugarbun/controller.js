const ObjectId = require('mongodb').ObjectID

const { getDB } = require('../../utils/mongo')

const ingredients = require('./collections/ingredients.json')
const cakeshape = require('./collections/cakeshape.json')
const cakesweight = require('./collections/cakesweight.json')
const adminstats = require('./collections/adminstats.json')

const INGREDIENTS_COLLECTION = 'ingredients'
const CAKESHAPE_COLLECTION = 'cakeshape'
const CAKESWEIGHT_COLLECTION = 'cakeswaight'
const ADMINSTATS_COLLECTION = 'adminstats'

let db = null

const connect = async () => {
    db = await getDB('sugarbundb')
}

const init = async () => {
    await connect()
    const ingredientsCollection = db.collection(INGREDIENTS_COLLECTION)
    const findingredients = await ingredientsCollection.find({
    }).toArray()
    // console.log(findingredients)
    if (findingredients.length === 0) {
        await ingredientsCollection.insertMany(ingredients)
    }
    const cakeshapeCollection = db.collection(CAKESHAPE_COLLECTION)
    const findcakeshape = await cakeshapeCollection.find({
    }).toArray()
    if (findcakeshape.length === 0) {
        await cakeshapeCollection.insertMany(cakeshape)
    }
    const cakesweightCollection = db.collection(CAKESWEIGHT_COLLECTION)
    const findcakesweight = await cakesweightCollection.find({
    }).toArray()
    if (findcakesweight.length === 0) {
        await cakesweightCollection.insertMany(cakesweight)
    }
    const adminstatsCollection = db.collection(ADMINSTATS_COLLECTION)
    const findadminstats = await adminstatsCollection.find({
    }).toArray()
    if (findadminstats.length === 0) {
        await adminstatsCollection.insertMany(adminstats.adminstats)
    }
}

init()

const getIngredients = async () => {
    if (db === null) throw new Error('no db connection')
    try {
        const ingredientsCollection = db.collection(INGREDIENTS_COLLECTION)
        const defaultIngredients = await ingredientsCollection.find({
            available: true,
        }).toArray()
        return (defaultIngredients)
    } catch (e) {
        throw new Error(e)
    }
}

const getCakeshape = async () => {
    if (db === null) throw new Error('no db connection')
    try {
        const cakeshapeCollection = db.collection(CAKESHAPE_COLLECTION)
        const defaultCakeshapes = await cakeshapeCollection.find({
            available: true,
        }).toArray()
        return (defaultCakeshapes)
    } catch (e) {
        throw new Error(e)
    }
}

const getCakesweight = async () => {
    if (db === null) throw new Error('no db connection')
    try {
        const cakesweightCollection = db.collection(CAKESWEIGHT_COLLECTION)
        const defaultCakesweights = await cakesweightCollection.find({
            available: true,
        }).toArray()
        return (defaultCakesweights)
    } catch (e) {
        throw new Error(e)
    }
}

const getStats = async () => {
    if (db === null) throw new Error('no db connection')
    try {
        const adminstatsCollection = db.collection(ADMINSTATS_COLLECTION)
        const defaultStats = await adminstatsCollection.find().toArray()
        return (defaultStats)
    } catch (e) {
        throw new Error(e)
    }
}

module.exports = {
    getIngredients,
    getCakeshape,
    getCakesweight,
    getStats,
}
