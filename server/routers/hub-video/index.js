const { getIo } = require('../../io')

const io = getIo()

io.on('connection', (socket) => {
    socket.on('play', () => {
        socket.broadcast.emit('play')
    })
})

console.log('test')
