const router = require('express').Router()

router.use('/dashboard', require('./dashboard'))
router.use('/landing', require('./landing'))
router.use('/categories', require('./categories'))
router.use('/shoppingList', require('./listItem'))
router.use('/auth', require('./auth'))

module.exports = router
