const router = require('express').Router()

router.get('/', (req, res) => {
    res.send(require('./json/landing/success.json'))
})

module.exports = router
