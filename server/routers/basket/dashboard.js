const router = require('express').Router()
const { expressjwt } = require('express-jwt')
const ObjectId = require('mongodb').ObjectID

const { BASKET_JWT_TOKEN } = require('./key')

const {
    getResponse, addList,
    getLists, deleteDoc, renameList, duplicateList,
} = require('./controller')

const wait = (req, res, next) => setTimeout(next, 0)

router.use(expressjwt({ secret: BASKET_JWT_TOKEN, algorithms: ['HS256'] }))

/* получить списки покупок*/
router.get('/list', wait, async (req, res) => {
    const userId = new ObjectId(req.auth.id)
    let error = null
    const listData = await getLists({ userId }).catch((e) => error = e.message)
    res.status(error ? 400 : 200).send(getResponse(error, listData))
})

/* удалить список*/
router.delete('/list', wait, async (req, res) => {
    let error = null
    const listData = await deleteDoc(req.body).catch((e) => error = e.message)
    res.status(error ? 400 : 200).send(getResponse(error, listData))
})

/* добавить новый список*/
router.post('/list', wait, async (req, res) => {
    const userId = new ObjectId(req.auth.id)
    let error = null
    // eslint-disable-next-line max-len
    const listData = await addList({ userId, ...req.body }).catch((e) => error = e.message)
    res.status(error ? 400 : 200).send(getResponse(error, listData))
})

/* переименовать список*/
router.put('/list', wait, async (req, res) => {
    let error = null
    const listData = await renameList(req.body).catch((e) => error = e.message)
    res.status(error ? 400 : 200).send(getResponse(error, listData))
})

/* дублировать список*/
router.post('/list/duplicate', wait, async (req, res) => {
    let error = null
    const listData = await duplicateList(req.body).catch((e) => error = e.message)
    res.status(error ? 400 : 200).send(getResponse(error, listData))
})

module.exports = router
