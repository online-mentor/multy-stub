const hash = require('pbkdf2-password')()

const users = [{
    salt: '1iKffJhuhGpQGx7JjiGc+ShnorhVcJDtEqTTBQjX7tnuMxfdN3nMpYpcxY0MZSPSCCGP+9l4swhit4E/ZGgVZA==',
    hash: 'tQ0V8293708V2/sjyVG6yMSAlizKmTezTpoLxBc0hpfxEOImRNRGQbEx3HuGrowE4HYjzdshJbu52E5rsDE0Hfrmy1unoL/8tjeWGRF04d2sF1lhUrpxt3v1pf3du0rhR2PLTL5d2BWEtT3pSStiWeetw/zdZRDNMo9PVQqnjX0=',
    login: 'task-boss',
    id: '1',
}]

function authenticate(login, pass, fn) {
    let user = users.find((u) => u.login === login)
    if (!user) return fn(new Error('невозможно найти пользователя'))
    hash({
        password: pass, salt: user.salt,
    }, (err, pass, salt, hash) => {
        if (err) return fn(err)
        if (hash === user.hash) return fn(null, user)
        fn(new Error('неверный пароль'))
    })
}

module.exports = authenticate
