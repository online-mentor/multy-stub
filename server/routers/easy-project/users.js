const router = require('express').Router()
const { expressjwt: jwt } = require('express-jwt')
const ObjectId = require('mongodb').ObjectID

const { JWT_SECRET } = require('./constants')
const { requiredFields, responseWrapper } = require('./utils')
const { getAllUsers } = require('./db')

router.get('/healthcheck', (req, res) => {
    res.send(true)
})

router.get('/getForSelect', jwt({
    secret: JWT_SECRET, algorithms: ['HS256'],
}), async (req, res, next) => {
    const userList = await getAllUsers()

    res.send(responseWrapper(undefined, userList))
})

module.exports = router
