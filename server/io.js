const { Server } = require('socket.io')
const { createServer } = require('http')

let io = null

module.exports.setIo = (app) => {
    const server = createServer(app)
    io = new Server(server, {})

    return server
}

module.exports.getIo = () => io
